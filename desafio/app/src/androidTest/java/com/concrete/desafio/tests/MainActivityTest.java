package com.concrete.desafio.tests;

import android.test.ActivityInstrumentationTestCase2;

import com.concrete.desafio.R;
import com.concrete.desafio.activities.MainActivity;
import com.concrete.desafio.models.DribbbleShot;

import static com.google.android.apps.common.testing.ui.espresso.Espresso.onData;
import static com.google.android.apps.common.testing.ui.espresso.Espresso.onView;
import static com.google.android.apps.common.testing.ui.espresso.Espresso.pressBack;
import static com.google.android.apps.common.testing.ui.espresso.action.ViewActions.click;
import static com.google.android.apps.common.testing.ui.espresso.assertion.ViewAssertions.matches;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.isDisplayed;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;


public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    public void testLoadMoreShots() {
        onView(withId(R.id.list_footer))
                .perform(click());

        onData(allOf(is(instanceOf(DribbbleShot.class))))
                .inAdapterView(withId(R.id.shot_list))
                .atPosition(19)
                .check(matches(isDisplayed()));
    }

    public void testCheckShotDetailsAndBack() {
        onData(allOf(is(instanceOf(DribbbleShot.class))))
                .inAdapterView(withId(R.id.shot_list))
                .atPosition(0)
                .perform(click());

        pressBack();
    }
}
