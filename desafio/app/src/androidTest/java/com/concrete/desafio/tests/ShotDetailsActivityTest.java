package com.concrete.desafio.tests;

import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.text.Html;

import com.concrete.desafio.R;
import com.concrete.desafio.activities.MainActivity;
import com.concrete.desafio.activities.ShotDetailsActivity;
import com.concrete.desafio.models.DribbblePlayer;
import com.concrete.desafio.models.DribbbleShot;

import org.parceler.Parcels;

import static com.google.android.apps.common.testing.ui.espresso.Espresso.onView;
import static com.google.android.apps.common.testing.ui.espresso.assertion.ViewAssertions.matches;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withId;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withText;

public class ShotDetailsActivityTest extends ActivityInstrumentationTestCase2<ShotDetailsActivity>{

    String playerName = "Tim Van Damme";
    String shotTitle = "End of an era";
    String shotDescription = "\"<p>After 5 years, I decided to retire the old \\\"Contact Card\\\" website. My HTML and CSS is rusty, but I managed to hack this page together and make it semi-responsive. Even includes @3x assets for those on an iPhone 6 Plus!</p>\\n\\n<p>http://timvandamme.com</p>\"";
    int viewCount = 11019;

    public ShotDetailsActivityTest() {
        super(ShotDetailsActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        DribbbleShot shot = new DribbbleShot();
        DribbblePlayer player = new DribbblePlayer();

        player.setAvatar_url("https://d13yacurqjgara.cloudfront.net/users/22/avatars/normal/2dfb3c406c547c6cab0b8feb19121941.jpg?1405633606");
        player.setName(playerName);
        shot.setPlayer(player);
        shot.setImage_url("https://d13yacurqjgara.cloudfront.net/users/22/screenshots/1757954/tvd.png");
        shot.setDescription(shotDescription);
        shot.setTitle(shotTitle);
        shot.setViews_count(viewCount);

        Intent intent = new Intent().putExtra(MainActivity.SHOT_PARCELABLE_TAG, Parcels.wrap(shot));
        setActivityIntent(intent);
        getActivity();
    }

    public void testShotDetails() {
        onView(withId(R.id.player_name))
                .check(matches(withText(playerName)));
        onView(withId(R.id.shot_description))
                .check(matches(withText(Html.fromHtml(shotDescription).toString())));
        onView(withId(R.id.view_count))
                .check(matches(withText(String.valueOf(viewCount))));
        onView(withId(R.id.shot_title))
                .check(matches(withText(shotTitle)));
    }
}
