package com.concrete.desafio.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.concrete.desafio.models.DribbbleShot;
import com.concrete.desafio.R;
import com.concrete.desafio.adapters.MainListAdapter;
import com.concrete.desafio.interfaces.DribbbleInterface;

import org.parceler.Parcels;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.RestAdapter;

public class MainActivity extends Activity {
    public final static String SHOT_PARCELABLE_TAG = "parcelableShot";

    private final static int PAGES_MAX = 50;

    @InjectView(R.id.shot_list) ListView shotList;

    MainListAdapter adapter;

    int nextPage = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        adapter = new MainListAdapter(getBaseContext());

        shotList.setAdapter(adapter);
        shotList.addFooterView(getLayoutInflater().inflate(R.layout.activity_main_list_footer, null), null, true);
        shotList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == adapter.getCount()) {
                    if(nextPage > 50) {
                        Toast.makeText(getBaseContext(), "No more shots to load.", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        new GetShotsTask().execute(nextPage);
                        nextPage++;
                    }
                } else {
                    DribbbleShot shot = (DribbbleShot) adapter.getItem(position);

                    Intent shotDetailsIntent = new Intent(getBaseContext(), ShotDetailsActivity.class)
                            .putExtra(SHOT_PARCELABLE_TAG, Parcels.wrap(shot));

                    startActivity(shotDetailsIntent);
                }
            }
        });

        new GetShotsTask().execute(1);
    }

    class GetShotsTask extends AsyncTask<Integer, Void, List<DribbbleShot>> {
        @Override
        protected List<DribbbleShot> doInBackground(Integer... params) {
            DribbbleInterface apiService = new RestAdapter.Builder()
                    .setEndpoint("http://api.dribbble.com")
                    .build()
                    .create(DribbbleInterface.class);

            return apiService.listPopularShots(params[0]).getShots();
        }

        @Override
        protected void onPostExecute(List<DribbbleShot> dribbbleShots) {
            adapter.appendToList(dribbbleShots);
        }
    }
}
