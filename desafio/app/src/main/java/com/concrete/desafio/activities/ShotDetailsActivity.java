package com.concrete.desafio.activities;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.concrete.desafio.models.DribbbleShot;
import com.concrete.desafio.R;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ShotDetailsActivity extends Activity {
    @InjectView(R.id.shot) ImageView shotImage;
    @InjectView(R.id.shot_title) TextView title;
    @InjectView(R.id.view_count) TextView viewCount;
    @InjectView(R.id.player_name) TextView playerName;
    @InjectView(R.id.player_picture) ImageView playerPicture;
    @InjectView(R.id.shot_description) TextView shotDescription;

    DribbbleShot shotDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_details);
        ButterKnife.inject(this);

        shotDetails = Parcels.unwrap(getIntent().getExtras().getParcelable(MainActivity.SHOT_PARCELABLE_TAG));

        Picasso.with(getBaseContext()).load(shotDetails.getImage_url()).resize(800, 600).into(shotImage);
        title.setText(shotDetails.getTitle());
        viewCount.setText(String.valueOf(shotDetails.getViews_count()));

        playerName.setText(shotDetails.getPlayer().getName());
        Picasso.with(getBaseContext()).load(shotDetails.getPlayer().getAvatar_url()).into(playerPicture);
        shotDescription.setText(Html.fromHtml(shotDetails.getDescription() != null ? shotDetails.getDescription() : ""));
        shotDescription.setMovementMethod(android.text.method.LinkMovementMethod.getInstance());
    }
}
