package com.concrete.desafio.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.concrete.desafio.models.DribbbleShot;
import com.concrete.desafio.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainListAdapter extends BaseAdapter {

    List<DribbbleShot> shotList = new ArrayList<>();
    Context mContext;

    public MainListAdapter(Context c) {
        mContext = c;
    }

    public void appendToList(List<DribbbleShot> list) {
        shotList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return shotList.size();
    }

    @Override
    public Object getItem(int position) {
        return shotList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        DribbbleShot currentShot = (DribbbleShot) getItem(position);

        if(convertView != null) {
            viewHolder = (ViewHolder) convertView.getTag();
        } else {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.activity_main_list_item, null);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        viewHolder.injectShot(currentShot, mContext);

        return convertView;
    }

    static class ViewHolder {
        @InjectView(R.id.shot) ImageView shotImage;
        @InjectView(R.id.shot_title) TextView title;
        @InjectView(R.id.view_count) TextView viewCount;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

        public void injectShot(DribbbleShot shot, Context context) {
            Picasso.with(context).load(shot.getImage_url()).resize(800, 600).into(shotImage);
            title.setText(shot.getTitle());
            viewCount.setText(String.valueOf(shot.getViews_count()));
        }
    }
}
