package com.concrete.desafio.interfaces;

import com.concrete.desafio.models.DribbbleResponse;

import retrofit.http.GET;
import retrofit.http.Query;

public interface DribbbleInterface {
    @GET("/shots/popular?per_page=10")
    DribbbleResponse listPopularShots(@Query("page") int page);
}
