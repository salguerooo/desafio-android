package com.concrete.desafio.models;

import java.util.List;

public class DribbbleResponse {
    String page;
    int per_page;
    int pages;
    List<DribbbleShot> shots;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<DribbbleShot> getShots() {
        return shots;
    }

    public void setShots(List<DribbbleShot> shots) {
        this.shots = shots;
    }
}
