package com.concrete.desafio.models;

import org.parceler.Parcel;

@Parcel
public class DribbbleShot {
    String title;
    String description;
    int views_count;
    String image_url;
    DribbblePlayer player;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getViews_count() {
        return views_count;
    }

    public void setViews_count(int views_count) {
        this.views_count = views_count;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public DribbblePlayer getPlayer() {
        return player;
    }

    public void setPlayer(DribbblePlayer player) {
        this.player = player;
    }
}
